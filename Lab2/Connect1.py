import psycopg2


# Function that returns connection to DB
def makeConnect():
    return psycopg2.connect(
        user="postgres",
        password="ibanez",
        host="localhost",
        port="5432",
        database="lab",
    )


# Function that closes connection to DB
def closeConnect(connection):
    connection.commit()
    connection.close()
