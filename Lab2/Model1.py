import random
import Connect1
from View1 import View

tables = {
    1: 'author',
    2: 'authorbook',
    3: 'book',
    4: 'lib_user',
    5: 'phone',
    6: 'publisher',
    7: 'publisher_adress',
    8: 'user_adress',
}

# Data to randomize 'lib_user' table
first_name = 'first_name'
last_name = 'last_name'
randomUser = {
    first_name: {
        1: 'Maria',
        2: 'Borys',
        3: 'Natalia',
        4: 'Mikhail',
        5: 'Anna',
        6: 'Paul',
        
    },
    last_name: {
        1: 'Brooks',
        2: 'Remark',
        3: 'Herrington',
        4: 'Trump',
        5: 'Jackson',
        7: 'Gatsby',
        
    },
}


class Model:
    # Method that checks valid of the number of table that user input and returns it
    @staticmethod
    def validTable():
        incorrect = True
        while incorrect:
            table = input('Choose table number => ')
            if table.isdigit():
                table = int(table)
                if table >= 1 and table <= 6:
                    incorrect = False
                else:
                    print('Incorrect input, try again.')
            else:
                print('Incorrect input, try again.')
        return table

        # Method that prints all of DB tables

    @staticmethod
    def showAllTables():
        connect = Connect1.makeConnect()
        cursor = connect.cursor()
        for table in range(1, 7):
            table_name = '''"''' + tables[table] + '''"'''
            print(tables[table])

            show = 'select * from public.{}'.format(table_name)

            print("SQL query => ", show)
            print('')
            cursor.execute(show)
            records = cursor.fetchall()
            obj = View(table, records)
            obj.show()
        cursor.close()
        Connect1.closeConnect(connect)

        # Method that prints one table

    @staticmethod
    def showOneTable():
        View.list()
        connect = Connect1.makeConnect()
        cursor = connect.cursor()

        table = Model.validTable()

        table_name = '''"''' + tables[table] + '''"'''
        print(tables[table])

        show = 'select * from public.{}'.format(table_name)

        print("SQL query => ", show)
        print('')
        cursor.execute(show)
        records = cursor.fetchall()
        obj = View(table, records)
        obj.show()
        cursor.close()
        Connect1.closeConnect(connect)
        
    @staticmethod
    def insert():
        connect = Connect1.makeConnect()
        cursor = connect.cursor()
        restart = True
        while restart:
            View.list()
            table = Model.validTable()
            if table == 1:
                scfirst = input("First name = ")
                sclast = input("Last name = ")
                scid = input("ID = ")                

                insert = 'insert into "author" ("first_name", "last_name", "author_id") values ({}, {}, {})'.format(
                    scfirst, sclast, scid)

                restart = False
            elif table == 2:
                clauthorid = input('author_id = ')
                clbookid = input('book_id = ')
                

                insert = 'insert into "authorbook" ("author_id", "book_id") values ({}, {})'.format(
                    clauthorid, clbookid)

                restart = False
            elif table == 3:
                diyear = input('Year of publishing = ')
                dititle = input('Title = ')
                diid = input('ID = ')
                dipublisher = input('Publisher ID = ')
                direader = input('Current reader ID = ')

                insert = 'insert into "book" ("publishing_year", "title", "book_id", "publisher", "current_reader_id") values ({}, {}, {}, {}, {})'.format(
                    diyear, dititle,
                    diid, dipublisher, direader)

                restart = False
            elif table == 4:
                teid = input('ID = ')
                tefirst_name = input('First name = ')
                telast_name = input('Last name = ')
                tepatronymic = input('Patronymic = ')

                insert = 'insert into "lib_user" ("user_id", "first_name", "last_name", "patronymic") values ({}, {}, {}, {})'.format(
                    teid, tefirst_name, telast_name, tepatronymic)

                restart = False
            elif table == 5:
                stid = input('ID = ')
                sttype = input('Type of phone (mobile/stationary) = ')
                stowner = input('Owner ID = ')
                stnumber = input('Phone number = ')
                

                insert = 'insert into "phone" ("phone_id", "type", "user_id", "number") values ({}, {}, {}, {})'.format(
                    stid, sttype, stowner, stnumber)

                restart = False
            elif table == 6:
                tdid = input('ID = ')
                tdname = input('Name = ')
                
                insert = 'insert into "publisher" ("id", "org_name") values ({}, {})'.format(
                    tdid, tdname)
                restart = False
            elif table == 7:
                pacountry = input('Country = ')
                pastate = input('State = ')
                pacity = input('City = ')
                pahouse = input('House = ')
                paapartment = input('Apartment = ')
                paid = input('ID = ')
                paowner = input('Owner ID = ')

                insert = 'insert into "publisher_adress" ("country", "state", "city", "house", "apartment", "publisher_id", "adress_id") values ({}, {}, {}, {}, {}, {}, {})'.format(
                    pacountry, pastate, pacity, pahouse, paapartment, paid, paowner)
                restart = False
            elif table == 8:
                uacountry = input('Country = ')
                uastate = input('State = ')
                uacity = input('City = ')
                uahouse = input('House = ')
                uaapartment = input('Apartment = ')
                uaid = input('ID = ')
                uaowner = input('Owner ID = ')

                insert = 'insert into "user_adress" ("country", "state", "city", "house", "apartment", "user_id", "adress_id") values ({}, {}, {}, {}, {}, {}, {})'.format(
                    uacountry, uastate, uacity, uahouse, uaapartment, uaid, uaowner)
                restart = False
            else:
                print('\nIncorrect input, try again.')
        print(tables[table])
        print('SQl query => ', insert)
        cursor.execute(insert)
        connect.commit()
        print('Data added successfully!')
        cursor.close()
        Connect1.closeConnect(connect)

# ----------TASK 1----------Deleting data from DB

    # Method that deletes data from DB
    @staticmethod
    def delete():
        connect = Connect1.makeConnect()
        cursor = connect.cursor()
        restart = True
        while restart:
            View.list()
            table = Model.validTable()

            if table == 1:
                scname = input('Attribute to delete Author = ')
                delete = 'delete from "author" where "author_id"= {}'.format(scid)
                restart = False
            elif table == 2:
                clname = input('Attribute to delete Author = ')
                delete = 'delete from "authorbook" where "author_id"=  {}'.format(clauthorid)
                restart = False
            elif table == 3:
                dsname = input('Attribute to delete Book = ')
                delete = 'delete from "book" where "book_id"= {}'.format(diid)
                restart = False
            elif table == 4:
                tsfull_name = input('Attribute to delete User = ')
                delete = 'delete from "lib_user" where "user_id"= {}'.format(teid)
                restart = False
            elif table == 5:
                sttype = input('Attribute to delete Phone = ')
                delete = 'delete from "phone" where "phone_id"=  {}'.format(stid)
                restart = False
            elif table == 6:
                tdid = "'" + input('Attribute to delete Publisher = ') + "'"
                delete = 'delete from "publisher" where "publisher_id"=  {}'.format(tdid)
                restart = False
            elif table == 7:
                rsname = "'" + input('Attribute to delete Publisher adress = ') + "'"
                delete = 'delete from "publisher_adress" where "adress_id"= {}'.format(paid)
                restart = False
            elif table == 8:
                trname = "'" + input('Attribute to delete User adress = ') + "'"
                delete = 'delete from "user_adress" where "adress_id"= {}'.format(uaid)
                restart = False
            else:
                print('\nIncorrect input, try again.')
        print(tables[table])
        print("SQL query => ", delete)
        cursor.execute(delete)
        connect.commit()
        print('Data deleted successfully!')
        cursor.close()
        Connect1.closeConnect(connect)

# ----------TASK 1----------Updating data in DB
   
    # Method that updates data in DB
    @staticmethod
    def update():
        connect = Connect1.makeConnect()
        cursor = connect.cursor()
        restart = True
        while restart:
            View.list()
            table = Model.validTable()
            if table == 1:
                scname = "'" + input('Attribute to update(where) author_id = ') + "'"
                View.attribute_list(1)
                in_restart = True
                while in_restart:
                    num = input('Number of attribute =>')
                    value = "'" + input('New value of attribute = ') + "'"
                    if num == '1':
                        set = '"first_name"= {}'.format(value)
                        in_restart = False
                    elif num == '2':
                        set = '"last_name"= {}'.format(value)
                        in_restart = False
                    elif num == '3':
                        set = '"author_id"= {}'.format(value)
                        in_restart = False
                    else:
                        print('\nIncorrect input, try again.')
                update = 'update "author" set {} where "author_id"= {}'.format(set, scid)
                restart = False
                pass
            elif table == 2:
                clname = "'" + input('Attribute to update(where) author_id = ') + "'"
                View.attribute_list(2)
                in_restart = True
                while in_restart:
                    num = input('Number of attribute =>')
                    value = "'" + input('New value of attribute = ') + "'"
                    if num == '1':
                        set = '"author_id"= {}'.format(value)
                        in_restart = False
                    elif num == '2':
                        set = '"book_id"= {}'.format(value)
                        in_restart = False                    
                    else:
                        print('\nIncorrect input, try again.')
                update = 'update "authorbook" set {} where "author_id"= {}'.format(set, clauthorid)
                restart = False
                pass
            elif table == 3:
                diname = "'" + input('Attribute to update(where) book_id = ') + "'"
                View.attribute_list(3)
                in_restart = True
                while in_restart:
                    num = input('Number of attribute =>')
                    value = "'" + input('New value of attribute = ') + "'"
                    if num == '1':
                        set = '"publishing_year"= {}'.format(value)
                        in_restart = False
                    elif num == '2':
                        set = '"title"= {}'.format(value)
                        in_restart = False
                    elif num == '3':
                        set = '"book_id"= {}'.format(value)
                        in_restart = False
                    elif num == '4':
                        set = '"publisher"= {}'.format(value)
                        in_restart = False
                    elif num == '5':
                        set = '"current_reader_id"= {}'.format(value)
                        in_restart = False
                    else:
                        print('\nIncorrect input, try again.')
                update = 'update "book" set {} where "book_id"= {}'.format(set, diid)
                restart = False
                pass
            elif table == 4:
                tename = input('Attribute to update(where) user_id = ')
                View.attribute_list(4)
                in_restart = True
                while in_restart:
                    num = input('Number of attribute =>')
                    value = "'" + input('New value of attribute = ') + "'"
                    if num == '1':
                        set = '"user_id"= {}'.format(value)
                        in_restart = False
                    elif num == '2':
                        set = '"first_name"= {}'.format(value)
                        in_restart = False
                    elif num == '3':
                        set = '"last_name"= {}'.format(value)
                        in_restart = False
                    elif num == '4':
                        set = '"patronymic"= {}'.format(value)
                        in_restart = False                    
                    else:
                        print('\nIncorrect input, try again.')
                update = 'update "lib_user" set {} where "user_id"= {}'.format(set, teid)
                restart = False
                pass
            elif table == 5:
                stname = input('Attribute to update(where) phone_id = ')
                View.attribute_list(5)
                in_restart = True
                while in_restart:
                    num = input('Number of attribute =>')
                    value = "'" + input('New value of attribute = ') + "'"
                    if num == '1':
                        set = '"phone_id"= {}'.format(value)
                        in_restart = False
                    elif num == '2':
                        set = '"type"= {}'.format(value)
                        in_restart = False
                    elif num == '3':
                        set = '"user_id"= {}'.format(value)
                        in_restart = False
                    elif num == '4':
                        set = '"number"= {}'.format(value)
                        in_restart = False
                    else:
                        print('\nIncorrect input, try again.')
                update = 'update "phone" set {} where "phone_id"= {}'.format(set, stid)
                restart = False
                pass
            elif table == 6:
                dtname = "'" + input('Attribute to update(where) publisher_id = ') + "'"
                View.attribute_list(6)
                in_restart = True
                while in_restart:
                    num = input('Number of attribute =>')
                    value = "'" + input('New value of attribute = ') + "'"
                    if num == '1':
                        set = '"org_name"= {}'.format(value)
                        in_restart = False
                    elif num == '2':
                        set = '"publisher_id"= {}'.format(value)
                        in_restart = False                    
                    else:
                        print('\nIncorrect input, try again.')
                update = 'update "publisher" set {} where "publisher_id"= {}'.format(set, tdid)
                restart = False
                pass
            elif table == 7:
                dtname = "'" + input('Attribute to update(where) adress_id = ') + "'"
                View.attribute_list(6)
                in_restart = True
                while in_restart:
                    num = input('Number of attribute =>')
                    value = "'" + input('New value of attribute = ') + "'"
                    if num == '1':
                        set = '"country"= {}'.format(value)
                        in_restart = False
                    elif num == '2':
                        set = '"state"= {}'.format(value)
                        in_restart = False
                    elif num == '3':
                        set = '"city"= {}'.format(value)
                        in_restart = False
                    elif num == '4':
                        set = '"house"= {}'.format(value)
                        in_restart = False
                    elif num == '5':
                        set = '"apartment"= {}'.format(value)
                        in_restart = False
                    elif num == '6':
                        set = '"publisher_id"= {}'.format(value)
                        in_restart = False
                    elif num == '7':
                        set = '"adress_id"= {}'.format(value)
                        in_restart = False
                    else:
                        print('\nIncorrect input, try again.')
                update = 'update "publisher_adress" set {} where "adress_id"= {}'.format(set, paid)
                restart = False
                pass
            elif table == 8:
                dtname = "'" + input('Attribute to update(where) adress_id = ') + "'"
                View.attribute_list(6)
                in_restart = True
                while in_restart:
                    num = input('Number of attribute =>')
                    value = "'" + input('New value of attribute = ') + "'"
                    if num == '1':
                        set = '"country"= {}'.format(value)
                        in_restart = False
                    elif num == '2':
                        set = '"state"= {}'.format(value)
                        in_restart = False
                    elif num == '3':
                        set = '"city"= {}'.format(value)
                        in_restart = False
                    elif num == '4':
                        set = '"house"= {}'.format(value)
                        in_restart = False
                    elif num == '5':
                        set = '"apartment"= {}'.format(value)
                        in_restart = False
                    elif num == '6':
                        set = '"user_id"= {}'.format(value)
                        in_restart = False
                    elif num == '7':
                        set = '"adress_id"= {}'.format(value)
                        in_restart = False
                    else:
                        print('\nIncorrect input, try again.')
                update = 'update "publisher_adress" set {} where "adress_id"= {}'.format(set, paid)
                restart = False
                pass
            else:
                print('\nIncorrect input, try again.')
        print(tables[table])
        print("SQL query => ", update)
        cursor.execute(update)
        connect.commit()
        print('Data updeted successfully!')
        cursor.close()
        Connect1.closeConnect(connect)
        pass

    # ----------TASK 3----------

    # Method that selects data from DB
    @staticmethod
    def select():
        connect = Connect1.makeConnect()
        cursor = connect.cursor()
        print('''
        You can select range.
        ''')
        print('You may enter integer range:')
        a = input('')
        b = input('')

        select = """
        select * from "book" 
        join "lib_user" on "user_id"."id" = "book"."user_id" 
        where {}<user.id and user.id<{};
        """.format(a, b)

        print("SQL query => ", select)
        cursor.execute(select)
        records = cursor.fetchall()
        obj = View(5, records)
        obj.showSelect()

        print('Data selected successfully!')
        cursor.close()
        Connect1.closeConnect(connect)

# Method that runs full text search in DB 

    @staticmethod
    def text_search():
        connect = Connect1.makeConnect()
        cursor = connect.cursor()
        restart = True
        while restart:
            View.list()
            table = Model.validTable()
            text = "'" + input('Search text = ') + "'"
            incorrect = True
            while incorrect:
                mode = input('''
                1 -- Word is not included
                2 -- Required word entry
                Choose mode = > ''')
                if mode.isdigit():
                    mode = int(mode)
                    if mode >= 1 and mode <= 2:
                        incorrect = False
                    else:
                        print('Incorrect input, try again.')
                else:
                    print('Incorrect input, try again.')

            if mode == 1:
                if table == 1:
                    text_search = 'select * from "author" where not (to_tsvector("author_id") @@ plainto_tsquery({}))'.format(
                        text)
                    restart = False
                elif table == 2:
                    text_search = 'select * from "authorbook" where not (to_tsvector("author_id") @@ plainto_tsquery({}))'.format(
                        text)
                    restart = False
                elif table == 3:
                    text_search = 'select * from "book" where not (to_tsvector("book_id") @@ plainto_tsquery({}))'.format(
                        text)
                    restart = False
                elif table == 4:
                    text_search = 'select * from "lib_user" where not (to_tsvector("user_id") @@ plainto_tsquery({}))'.format(
                        text)
                    restart = False
                elif table == 5:
                    text_search = 'select * from "phone" where not (to_tsvector("phone_id") @@ plainto_tsquery({}))'.format(
                        text)
                    restart = False
                elif table == 6:
                    text_search = 'select * from "publisher" where not (to_tsvector("publisher_id") @@ plainto_tsquery({}))'.format(
                        text)
                    restart = False
                elif table == 7:
                    text_search = 'select * from "publisher_adress" where not (to_tsvector("adress_id") @@ plainto_tsquery({}))'.format(
                        text)
                    restart = False
                elif table == 8:
                    text_search = 'select * from "user_adress" where not (to_tsvector("adress_id") @@ plainto_tsquery({}))'.format(
                        text)
                    restart = False
                else:
                    print('\nIncorrect input, try again.')
            elif mode == 2:
                if table == 1:
                    text_search = 'select * from "school" where to_tsvector("author_id") @@ plainto_tsquery({})'.format(
                        text)
                    restart = False
                elif table == 2:
                    text_search = 'select * from "classes" where to_tsvector("author_id") @@ plainto_tsquery({})'.format(
                        text)
                    restart = False
                elif table == 3:
                    text_search = 'select * from "discipline" where to_tsvector("book_id") @@ plainto_tsquery({})'.format(text)
                    restart = False
                elif table == 4:
                    text_search = 'select * from "teachers" where to_tsvector("user_id") @@ plainto_tsquery({})'.format(
                        text)
                    restart = False
                elif table == 5:
                    text_search = 'select * from "students" where to_tsvector("phone_id") @@ plainto_tsquery({})'.format(
                        text)
                    restart = False
                elif table == 6:
                    text_search = 'select * from "students" where to_tsvector("publisher_id") @@ plainto_tsquery({})'.format(
                        text)
                    restart = False
                elif table == 7:
                    text_search = 'select * from "students" where to_tsvector("adress_id") @@ plainto_tsquery({})'.format(
                        text)
                    restart = False
                elif table == 8:
                    text_search = 'select * from "students" where to_tsvector("adress_id") @@ plainto_tsquery({})'.format(
                        text)
                    restart = False
                else:
                    print('\nIncorrect input, try again.')
            else:
                print('\nIncorrect input, try again.')

        print(tables[table])
        print('SQL query => ', text_search)
        cursor.execute(text_search)
        records = cursor.fetchall()
        obj = View(table, records)
        obj.show()
        print('Data searched successfully!')
        cursor.close()
        Connect1.closeConnect(connect)

    # ----------TASK2----------

    # Method that randoms data into Client table
    @staticmethod
    def random():
        connect = Connect1.makeConnect()
        cursor = connect.cursor()

        incorrect = True
        while incorrect:
            num = input('How many users to random? => ')
            if num.isdigit():
                num = int(num)
                if num >= 1:
                    incorrect = False
                else:
                    print('Incorrect input, try again.')
            else:
                print('Incorrect input, try again.')

        for i in range(1, num + 1):
            randomFirst = "'" + randomUser[first_name][random.randint(1, 6)] + "'"
            randomLast = "'" + randomUser[last_name][random.randint(1, 7)] + "'"
            insert = 'insert into "lib_user" ("first_name", "last_name", "patronymic", "user_id") values ({}, {}, DEFAULT, DEFAULT)'.format(
                randomFirst, randomLast)

            print("SQL query => ", insert)
            cursor.execute(insert)
            connect.commit()

        print('Data randomised successfully!')
        cursor.close()
        Connect1.closeConnect(connect)