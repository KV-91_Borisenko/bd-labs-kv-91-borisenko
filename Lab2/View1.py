import Connect1



class View:

    # Initialization of class View
    def __init__(self, table, records):
        self.table = table
        self.records = records

    # Method that prints the list of DB tables
    @staticmethod
    def list():
        print('''
        1 => author
        2 => authorbook
        3 => book
        4 => lib_user
        5 => phone
        6 => publisher
        7 => publisher_adress
        8 => user_adress
        ''')

    # Method that prints the list of attributes of the selected table
    @staticmethod
    def attribute_list(table):
        if table == 1:
            print('''
            1 => first_name
            2 => last_name
            3 => author_id
            ''')
        elif table == 2:
            print('''
            1 => author_id
            2 => book_id
            ''')
        elif table == 3:
            print('''
            1 => publishing_year
            2 => title
            3 => book_id
            4 => publisher
            5 => current_reader_id
            ''')
        elif table == 4:
            print('''
            1 => user_id
            2 => first_name
            3 => last_name
            4 => patronymic
            ''')
        elif table == 5:
            print('''
            1 => phone_id
            2 => type
            3 => user_id
            4 => number
            ''')
        elif table == 6:
            print('''
            1 => org_name
            2 => publisher_id
            ''')
        elif table == 7:
            print('''
            1 => country
            2 => state
            3 => city
            4 => house
            5 => apartment
            6 => publisher_id
            7 => adress_id
            ''')
        elif table == 8:
            print('''
            1 => country
            2 => state
            3 => city
            4 => house
            5 => apartment
            6 => user_id
            7 => adress_id
            ''')
        

    # Method that prints content from a selected table
    def show(self):
        print("____________________\n")
        if self.table == 1:
            for row in self.records:
                print("First name = ", row[0])
                print("Last name = ", row[1])
                print("ID = ", row[2])
                print("____________________\n")
        elif self.table == 2:
            for row in self.records:
                print("Author ID = ", row[0])
                print("Book ID = ", row[1])
                print("____________________\n")
        elif self.table == 3:
            for row in self.records:
                print("Publishing year = ", row[0])
                print("Title = ", row[1])
                print("ID = ", row[2])
                print("Publisher = ", row[3])
                print("Current reader = ", row[4])
                print("____________________\n")
        elif self.table == 4:
            for row in self.records:
                print("ID = ", row[0])
                print("First name = ", row[1])
                print("Last name = ", row[2])
                print("Patronymic = ", row[3])
                print("____________________\n")
        elif self.table == 5:
            for row in self.records:
                print("ID = ", row[0])
                print("Type = ", row[1])
                print("User's ID = ", row[2])
                print("Number = ", row[3])
                print("____________________\n")
        elif self.table == 6:
            for row in self.records:
                print("Organization's Name = ", row[0])
                print("ID = ", row[1])
                print("____________________\n")
        elif self.table == 7:
            for row in self.records:
                print("Country = ", row[0])
                print("State = ", row[1])
                print("City = ", row[2])
                print("House = ", row[3])
                print("Apartment = ", row[4])
                print("Owner ID = ", row[5])
                print("ID = ", row[6])
                print("____________________\n")
        elif self.table == 8:
            for row in self.records:
                print("Country = ", row[0])
                print("State = ", row[1])
                print("City = ", row[2])
                print("House = ", row[3])
                print("Apartment = ", row[4])
                print("Owner ID = ", row[5])
                print("ID = ", row[6])
                print("____________________\n")

# Method that prints the result of select query

    def showSelect(self):
        for row in self.records:
            print("author_id = ", row[0])
            print("author_id = ", row[1])
            print("book_id = ", row[2])
            print("user_id = ", row[3])
            print("phone_id = ", row[4])
            print("publisher_id = ", row[5])
            print("adress_id = ", row[6])
            print("adress_id = ", row[7])
            print("____________________\n")

    # ----------TASK 4----------
