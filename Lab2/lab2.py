
import psycopg2
import Connect1
from Menu1 import Menu

# Catching errors
try:
    # Making connection
    connection = Connect1.makeConnect()
    # Creating cursor to control DB
    cursor = connection.cursor()
    # Controller call
    Menu.mainmenu()

except (Exception , psycopg2.Error) as error :
        print ("PostgreSQL Error: ",error)
finally:
    # Closing connection and cursor
    cursor.close()
    connection.close()
    print("PostgreSQL connection is closed")